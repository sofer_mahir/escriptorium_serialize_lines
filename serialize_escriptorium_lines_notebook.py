from dataclasses import dataclass
from operator import itemgetter
import pandas as pd
from shapely.geometry import Polygon, LineString
from tqdm.notebook import tqdm


@dataclass(init=True, frozen=True)
class SerializedData:
    __slots__ = (
        "incremental_counter",
        "document_pk",
        "part_pk",
        "transcription_pk",
        "region_pk",
        "region_order",
        "line_pk",
        "line_order",
        "line_type",
        "line_baseline",
        "line_mask",
        "line_transcription_pk",
        "line_transcription_content",
    )
    incremental_counter: int
    document_pk: int
    part_pk: int
    transcription_pk: int
    region_pk: int
    region_order: int
    line_pk: int
    line_order: int
    line_type: int
    line_baseline: str
    line_mask: str
    line_transcription_pk: int
    line_transcription_content: str


def serialize_escriptorium_document_part(
    document_pk: int,
    part_pk: int,
    transcription_pk: int,
    conn,
) -> pd.DataFrame:
    """Gathers the data for the submitted document, part, and transcription PKs
    and serialized the transcription and spatial information into a single dataframe.

    Args:
        document_pk (int): The primary key of the document to be serialized
        part_pk (int): The primary key of the part in the document to be serialized
        transcription_pk (int): The primary key of the transcription level to be serialized
        conn (Union[EscriptoriumConnector, None], optional): An instance of the escriptorium python connector.
        If none is provided this function will try to create one based on the environment variables:
        ESCRIPTORIUM_URL and ESCRIPTORIUM_TOKEN. Defaults to None.

    Returns:
        pd.DataFrame: A dataframe containing the serialized data. The layout matches
        an array of `SerializedData` objects.
    """

    response: list[SerializedData] = []

    part_data = conn.get_document_part(document_pk, part_pk)
    regions = {part["pk"]: part["order"] for part in part_data["regions"]}

    lines = sorted(
        [
            {
                "pk": line["pk"],
                "order": line["order"],
                "region": line["region"],
                "region_order": regions[line["region"]],
                "baseline": LineString(line["baseline"]).wkt,
                "mask": Polygon(line["mask"]).wkt,
                "typology": line["typology"],
            }
            for line in part_data["lines"]
        ],
        key=itemgetter("region_order", "order"),
    )

    def get_line_transcription(line_pk: int):
        line_transcription = conn.get_document_part_line_transcription_by_transcription(
            document_pk, part_pk, line_pk, transcription_pk
        )
        if line_transcription is None:
            return 0, ""
        return line_transcription["pk"], line_transcription["content"]

    response = [
        SerializedData(
            idx,
            document_pk,
            part_pk,
            transcription_pk,
            line["region"],
            line["region_order"],
            line["pk"],
            line["order"],
            line["typology"],
            line["baseline"],
            line["mask"],
            *get_line_transcription(line["pk"]),
        )
        for idx, line in tqdm(
            enumerate(lines),
            total=len(lines),
            colour="blue",
            desc="Getting data for lines",
            leave=False,
        )
    ]

    return pd.DataFrame(response)
