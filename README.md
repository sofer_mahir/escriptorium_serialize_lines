# escriptorium_serialize_lines

This python lib will download the information pertaining to a document/part/transcription in eScriptorium into a flat CSV representation of the data.

## Installation

Make sure you are using a modern version of Python, perhaps 3.7 or greater.

Clone this git repo. Enter the new directory and run `pip install -r requirements.txt`.

You will need to add your own `.env` file containing the eScriptorium server address and your access credentials.  Here is an example:
```bash
ESCRIPTORIUM_URL=https://escriptorium.fr
ESCRIPTORIUM_TOKEN=your-escriptorium-token
``` 

If you do not know your eScriptorium token, please go to your eScriptorium server (maybe https://escriptorium.fr).  Login to your account, then click on your username in the top right-hand side of the screen, click on "profile", then select "ApiKey". Click on the little clipboard icon to properly copy your API key. When you add it to your `.env` file, please do not add any spaces between it and the `=` sign, and do not any any spaces after it.

## Usage

### Standalone

This utility can be used directly from the command line by specifying the document, part, and transcription PKs (and optionally the `out` folder.) To see the options use `python serialize_escriptorium_lines.py -h`. A possible example usage would be `python serialize_escriptorium_lines.py -d 694 -p 177405 -t 1330`, which would download from document 694, part 177405 all line transcriptions that are part of the transcription 1330. It will place the output CSV into `./serialized-694-177405-1330.csv`.

### As a library

You can import `serialize_escriptorium_document_part` into your existing Python project and use it as follows:
```python
from dotenv import load_dotenv
import csv


url = os.getenv("ESCRIPTORIUM_URL")
api = f"{url}/api"
token = os.getenv("ESCRIPTORIUM_TOKEN")
conn = EscriptoriumConnector(url, api, token)

document = 694
part = 177405
transcription = 1330
serialized_data = serialize_escriptorium_document_part(
        document, part, transcription, conn
    )

out_file = Path("./out")
serialized_data.to_csv(out_file, index=False, quoting=csv.QUOTE_NONNUMERIC)
```