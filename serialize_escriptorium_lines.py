#!/usr/bin/env python3


import argparse
from colored import stylize, fore, back, style
import csv
from escriptorium_python_connector.src.escriptorium_connector import (
    EscriptoriumConnector,
)
from dataclasses import dataclass
import logging
from operator import itemgetter
import os
import pandas as pd
from pathlib import Path
from shapely.geometry import Polygon, LineString
import sys
from tqdm import tqdm
from typing import Union


@dataclass(init=True, frozen=True)
class SerializedData:
    __slots__ = (
        "incremental_counter",
        "document_pk",
        "part_pk",
        "transcription_pk",
        "region_pk",
        "region_order",
        "line_pk",
        "line_order",
        "line_type",
        "line_baseline",
        "line_mask",
        "line_transcription_pk",
        "line_transcription_content",
    )
    incremental_counter: int
    document_pk: int
    part_pk: int
    transcription_pk: int
    region_pk: int
    region_order: int
    line_pk: int
    line_order: int
    line_type: int
    line_baseline: str
    line_mask: str
    line_transcription_pk: int
    line_transcription_content: str


def serialize_escriptorium_document_part(
    document_pk: int,
    part_pk: int,
    transcription_pk: int,
    conn: Union[EscriptoriumConnector, None] = None,
) -> pd.DataFrame:
    """Gathers the data for the submitted document, part, and transcription PKs
    and serialized the transcription and spatial information into a single dataframe.

    Args:
        document_pk (int): The primary key of the document to be serialized
        part_pk (int): The primary key of the part in the document to be serialized
        transcription_pk (int): The primary key of the transcription level to be serialized
        conn (Union[EscriptoriumConnector, None], optional): An instance of the escriptorium python connector.
        If none is provided this function will try to create one based on the environment variables:
        ESCRIPTORIUM_URL and ESCRIPTORIUM_TOKEN. Defaults to None.

    Returns:
        pd.DataFrame: A dataframe containing the serialized data. The layout matches
        an array of `SerializedData` objects.
    """

    log = logging.getLogger(__name__)
    response: list[SerializedData] = []

    if conn is None:
        log.debug("No escriptorium connection provided, creating one automatically.")
        from dotenv import load_dotenv

        load_dotenv()
        url = os.getenv("ESCRIPTORIUM_URL")
        if url is None:
            error = "Could not read the environment variable ESCRIPTORIUM_URL. Please add 'ESCRIPTORIUM_URL=https://...' to your .env file"
            log.error(error)
            print(stylize(error, fore.RED))
            sys.exit(1)

        api = f"{url}/api"
        token = os.getenv("ESCRIPTORIUM_TOKEN")
        if token is None:
            error = "Could not read the environment variable ESCRIPTORIUM_TOKEN. Please add 'ESCRIPTORIUM_TOKEN=your-escriptorium-token' to your .env file"
            log.error(error)
            print(stylize(error, fore.RED))
            sys.exit(1)

        conn = EscriptoriumConnector(url, api, token)
        log.debug("Escriptorium connection successfully created.")

    part_data = conn.get_document_part(document_pk, part_pk)
    regions = {part["pk"]: part["order"] for part in part_data["regions"]}

    lines = sorted(
        [
            {
                "pk": line["pk"],
                "order": line["order"],
                "region": line["region"],
                "region_order": regions[line["region"]],
                "baseline": LineString(line["baseline"]).wkt,
                "mask": Polygon(line["mask"]).wkt,
                "typology": line["typology"],
            }
            for line in part_data["lines"]
        ],
        key=itemgetter("region_order", "order"),
    )

    def get_line_transcription(line_pk: int) -> tuple[int, str]:
        line_transcription = conn.get_document_part_line_transcription_by_transcription(
            document_pk, part_pk, line_pk, transcription_pk
        )
        if line_transcription is None:
            return 0, ""
        return line_transcription["pk"], line_transcription["content"]

    response = [
        SerializedData(
            idx,
            document_pk,
            part_pk,
            transcription_pk,
            line["region"],
            line["region_order"],
            line["pk"],
            line["order"],
            line["typology"],
            line["baseline"],
            line["mask"],
            *get_line_transcription(line["pk"]),
        )
        for idx, line in tqdm(
            enumerate(lines),
            total=len(lines),
            colour="blue",
            desc="Getting data for lines",
            leave=False,
        )
    ]

    return pd.DataFrame(response)


def main(args):
    logging.debug(args)

    output_directory = Path(args.output_directory)
    if not output_directory.exists():
        output_directory.mkdir()

    if not output_directory.is_dir():
        error = stylize(
            f'{output_directory.resolve()} is not a directory. Cannot continue without a valid output directory (try "./").',
            fore.RED,
        )
        logging.error(error)
        print(error)
        sys.exit(1)

    document = int(args.document)
    part = int(args.part)
    transcription = int(args.transcription)

    out_file = output_directory / f"serialized-{document}-{part}-{transcription}.csv"

    print(
        stylize(
            f"Preparing to serialize document {document}, part {part}, transcription {transcription} into: {out_file.resolve()}",
            fore.BLUE,
        )
    )

    serialized_data = serialize_escriptorium_document_part(
        document, part, transcription
    )
    serialized_data.to_csv(out_file, index=False, quoting=csv.QUOTE_NONNUMERIC)

    print(
        stylize(
            f"Finished writing csv: {out_file.resolve()}",
            fore.GREEN,
        )
    )


if __name__ == "__main__":
    logging.basicConfig(
        filename=f"{Path(__file__).stem}.log",
        encoding="utf-8",
        level=logging.DEBUG,
        filemode="w",
    )

    parser = argparse.ArgumentParser(
        description="A simple CLI utility to serialize text lines from eScriptorium"
    )
    parser.add_argument(
        "-o",
        "--output_directory",
        help="The directory in which the serialized XML files will be stored",
        type=str,
        default="./out",
    )
    parser.add_argument(
        "-d",
        "--document",
        help="The pk of the document to be serialized",
        type=int,
        default=0,
    )
    parser.add_argument(
        "-p",
        "--part",
        help="The pk of the part to be serialized",
        type=int,
        default=0,
    )
    parser.add_argument(
        "-t",
        "--transcription",
        help="The pk of the transcription to be serialized",
        type=int,
        default=0,
    )
    args = parser.parse_args()
    main(args)
